<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

    <style>
        body {
            background-image: url('../resources/image/main_bg.jpg'),
                linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4));
            background-position: center;
            background-size: cover;
            background-repeat: repeat-y;
            height: 100%;
        }

        .navBG {
            background-color: rgba(6, 56, 149, 0.5);
            transition: all 0.5s;
        }

        .navBG:hover {
            background-color: rgb(3, 49, 136);
            cursor: pointer;
        }

        .searchBG {
            background-color: rgba(6, 56, 149, 0.5);
        }

        #pic1 {
            background-image: url();
            background-size: cover;
            background-position: center;
        }

        #pic2 {
            background-image: url('');
            background-size: cover;
            background-position: center;
        }

        #pic3 {
            background-image: url('');
            background-size: cover;
            background-position: center;
        }

        .socialicon {
            color: rgb(7, 109, 156);
            margin: 30px 5px;

        }

    </style>
</head>

<body>

    <nav class="navbar navbar-expand-sm navbar-dark navBG py-0 fixed-top">
        <h1 class="navbar-brand">
            <a class="nav-link" style="color: rgb(80, 97, 245)" href="#banner">WindInMov</a>
        </h1>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="menu" class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <form class="form-inline">
                    <input class="form-control mr-sm-2 searchBG" type="search" placeholder="Search" />
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                        Search
                    </button>
                </form>
                <li class="nav-item" id="b1">
                    <a class="nav-link" href="#product">最新影片</a>
                </li>
                <li class="nav-item" id="b2">
                    <a class="nav-link" href="#about">關於我們</a>
                </li>
                <li class="nav-item" id="b3">
                    <a class="nav-link" href="#contact">聯絡方式</a>
                </li>
                <li class="nav-item" id="b4">
                    <a class="nav-link" href="#footer">登入</a>
                </li>
            </ul>
        </div>
    </nav>



    <div class="container-fluid p-0">
      <div class="embed-responsive embed-responsive-16by9
      ">
        <video class="embed-responsive-item" src="../resources/video/ghost.mp4" loop autoplay muted allowfullcsreen></video>
      </div>

        <div class="container-fluid">
            <div id="carouselExampleIndicators" class="carousel slide row  p-0 my-5" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src='../resources/image/carousel1.jpg' class="d-block w-100" />
                    </div>
                    <div class="carousel-item">
                        <img src='../resources/image/carousel2.jpg' class="d-block w-100" />
                    </div>
                    <div class="carousel-item">
                        <img src='../resources/image/carousel3.jpg' class="d-block w-100" />
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="card-deck">
                    {{-- <div class="card">
                        <img src='' class="card-img-top" >

                    </div> --}}
                    <div class="card card-img bg-dark text-white" style="height: 18rem;">
                        <img src="../resources/image/card1.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay">
                          <h1 class="card-title  display-4 text-white font-weight-bolder">1</h1>


                          </div>
                      </div>
                    <div class="card">
                        <img src='../resources/image/card2.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card3.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card4.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card5.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card6.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card7.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card8.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>

                </div>
            </div>
            <div class="row my-5">
                <div class="card-deck">
                    <div class="card">
                        <img src='../resources/image/card9.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card10.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card11.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card12.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card13.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card14.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card15.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                    <div class="card">
                        <img src='../resources/image/card16.jpg' class="card-img-top" alt="" height="300px">
                        <div class="card-body bg-dark">
                            <h5 class="card-title text-light text-center">
                                <i class="fas fa-play-circle"></i>
                            </h5>
                            <p class="card-text"></p>
                        </div>
                        <div class="card-footer bg-secondary"><small class="text-muted"></small></div>
                    </div>
                </div>
            </div>

            <div class="row my-5">

                <div class="col-sm-2 offset-sm-3 mb-3"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2 offset-sm-3 mb-3"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2 offset-sm-3 mb-1"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
                <div class="col-sm-2"><a href="">法律聲明</a></div>
            </div>
            <div class="row mt-0 p-0">
                <div class="col-sm-2 offset-sm-3 mb-3"> <i class="fab fa-2x fa-twitter socialicon"></i></div>
                <div class="col-sm-2"> <i class="fab fa-2x fa-facebook socialicon"></i></div>
                <div class="col-sm-2"><i class="fab fa-2x fa-instagram socialicon"></i></div>
                <div class="col-sm-2"><i class="fab fa-2x fa-youtube socialicon"></i></div>

                <p class="col-6 offset-5">&copy;Allrights reserved | Design:YOUR LOGO 2020</p>
            </div>

</body>

</html>
